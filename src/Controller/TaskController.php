<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TaskController extends AbstractController
{
    #[Route('/taches', name: 'task_list', methods: 'GET')]
    public function taskList(): Response
    {
        return $this->render('tasks/task_list.html.twig');
    }
}