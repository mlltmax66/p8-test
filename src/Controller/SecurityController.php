<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    public function __construct(readonly private AuthenticationUtils $authenticationUtils)
    {
    }

    #[Route('/se-connecter', name: 'login', methods: ['GET', 'POST'])]
    public function login(): Response
    {
        $error = $this->authenticationUtils->getLastAuthenticationError();
        $lastUserName = $this->authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'lastUserName' => $lastUserName,
            'error' => $error,
        ]);
    }
}
