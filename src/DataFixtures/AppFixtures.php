<?php

namespace App\DataFixtures;

use App\Factory\TaskFactory;
use App\Factory\UserFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $adminUser = UserFactory::createOne(['email' => 'admin@admin.com', 'roles' => ['ROLE_ADMIN']]);
        TaskFactory::createMany(10, function () use ($adminUser) {
            return [
                'user' => $adminUser,
            ];
        });
        UserFactory::createMany(4);
        TaskFactory::createMany(40, function () {
            return [
                'user' => UserFactory::random(),
            ];
        });
    }
}
